\section{Approfondimento - Come nasce una piattaforma della P.A.?}
Nel ginepraio di regolamentazioni e di burocrazia che ci circonda, è quasi all'ordine del giorno trovarsi a seguire delle procedure o delle regole che non capiamo appieno o che comunque necessitano di non poca conoscenza e ricerca per essere padroneggiate. Questa sensazione di smarrimento e sedicente trasparenza mi ha accompagnato durante la maggior parte delle ricerche necessarie a scrivere queste poche righe. Le informazioni non sono nascoste o carenti, semplicemente ce ne sono troppe ed in troppi posti posti diversi.

Viviamo un'Italia in cui i servizi digitali sono numerosi e ne spuntano sempre di nuovi accompagnati altrettanto spesso da critiche, crash o fallimenti di varia natura.

Il rilascio delle piattaforme avviene spesso in giornate specifiche (click day) in cui la neonata piattaforma subisce un picco di utenza ingente, prevedibile ma mal gestito. Le soluzioni fantasiose non mancano (Fila digitale, implementazione dell'ultimo minuto di CDN che aprono falle di sicurezza da capogiro).
\\ \\
La domanda che ci si è posti è quindi semplice:
chi c'è dietro allo sviluppo (nel suo significato più ampio) di queste piattaforme?
Come mai i risultati sono così deboli e comunque colmi di ingenuità che non dovrebbero esistere a questi livelli?
\\
\\
Il primo documento a darci un indizio è \cite{ictLines}:
\begin{quote}
    \emph{
        È utile, in questa premessa, ricordare che la maggioranza dei contratti pubblici che riguardano l'ICT:
        \begin{itemize}
            \item derivano \textbf{da una gara} o rappresentano appalti specifici di \textbf{accordi quadro};
            \item sono pluriennali (per cui un certo grado di avvicendamento del personale del fornitore è inevitabile);
            \item comprendono più di un'iniziativa progettuale, in genere numerosi progetti distinti, che vengono condotti in parte sequenzialmente, in parte in parallelo, non necessariamente dallo stesso gruppo di lavoro del fornitore;
        \end{itemize}
    }
\end{quote}

Sembrerebbe quindi che diverse strade siano percorribili,
Si sono quindi studiati alcuni casi problematici recenti.
\begin{description}
    \item[Bonus Mobilità] In \cite{sogei} emerge che Sogei, l'azienda incaricata dello sviluppo, \emph{"è stata interamente acquisita dal Ministero dell’Economia e delle Finanze"} ed è riconosciuta secondo la Legge n. 44/12 \emph{"quale
              esclusivo partner tecnologico del Ministero dell'Economia e delle Finanze"}. L'azienda ha anche sviluppato altre piattaforme (18 App, Carta Del Docente, Carta della famiglia, Bonus Seggiolino) \cite{altriProgetti}.

          Il collasso della piattaforma, avvenuto nel giorno del rilascio, è stato attributo dalla stessa azienda (in un cominucato ad oggi rimosso ma raggiungibile attraverso \emph{internet archive}) non ad un fallimento della propria infrastruttura bensì all'ISP che avendo identificato un traffico anomalo ha bloccato le connessioni verso il sito \cite{down}

    \item[Immuni] Considerata l'urgenza del caso si è proceduto con una "fast call", una sorta di gara rapida (è rimasta aperta solo tre giorni). E si rivolgeva solo a coloro i quali avessero già realizzato la soluzione tecnologica richiesta. Era il caso appunto di Bending Spoons che sembrerebbe fosse partita in vantaggio avendo già sviluppato parte di software compatibile con la tecnologia  PEPP-PT (Successivamente abbandonata in funzione di una tecnologia basata su BLE resa possibile dal supporto nativo offerto da Google e Apple). Le motivazioni precise che hanno portato a questa scelta sono state attribuite ad una task force \cite{taskforce} e sembrerebbero non meglio motivate.

    \item[INPS] In \cite{inps} si legge \emph{"[Il settore IT] È gestito da personale interno dell'Istituto con specifica competenza tecnica, assistito dai maggiori partner tecnologici di mercato."}. È anche presente un riassunto dei fondi stanziati e delle aziende coinvolte negli ultimi anni (tanti i nomi fra cui spiccano:Accenture,Telecom Italia e addirittura IBM).

          Delle problematiche (recenti e non) legate ai servizi digitali offerti da INPS se ne è parlato a lungo.
          Soffermandosi sul più recente \emph{Bonus 600 Euro}  risulta difficile trovare delle informazioni prive di bias. Le informazioni ufficiali (escludendo quelle poco credibili che attribuiscono il tutto ad \emph{"un'attacco hacker"} qualunque cosa voglia dire) si perdono nel "rumore" generato dai titoli delle varie testate. Sembrerebbe che tale \emph{Vincenzo Di Nicola}, sia ora a capo dei progetti IT in INPS. Del suo (condivisibilissimo) parere sull'argomento (condito da un curriculum che definirei  \emph{consapevole}) si può leggere in \cite{diNicola}\footnote{Fa sorridere il che dopo aver criticato il codice HTML e la UI altrui sia sfuggito che il video sia per metà fuori dalla pagina. Si spera in una banale svista!}

    \item[App IO] L'app IO è sviluppata da PagoPa.  \emph{
              "PagoPA S.p.A. è una società partecipata dallo Stato creata allo scopo di diffondere i servizi digitali in Italia."}\cite{pagopa}

          L'app, disponibile soltanto su cellulari, ha avuto di recente problemi di carico dovuti al picco di utenza correlato all'introduzione del Cashback di stato. I motivi del fallimento sono ben trattati in \cite{medium} e quasi comprensibili considerata la narrazione (anche tecnica) portata avanti nell'articolo.\footnote{Si noti che l'articolo è pubblicato su un sito non istituzionale nonostante quello di PagoPA abbia una sezione apposita per i comunicati stampa. Il ritrovamento del link ha richiesto un po' di caparbietà nel voler trovare una fonte più "ufficiale". Dopo un po' di query e una ventina di articoli sfogliati (rigorosamente senza fonti) si è giunti a \url{https://www.dday.it/redazione/38042/cashback-app-io-problemi} e da qui all'articolo in questione }
\end{description}
In definitiva il sistema che va per la maggiore sembra essere quello delle gare d'appalto, sebbene il ricorso a  questo mezzo viene evitato nel caso in cui ci siano già delle risorse allocate. Bisognerebbe quindi interrogarsi sul processo di valutazione in seno alle gare. Argomento che meriterebbe un approfondimento dedicato lungo svariate pagine e conoscenze ben oltre quelle del sottoscritto.

Quello che però rimane è la relativa difficoltà nel reperire queste poche informazioni. Di fatto nulla ( o quasi) è nascosto, ma in pratica reperire alcune informazioni richiede se non altro di sapere precisamente dove andare a cercare. Anche nei documenti ufficiali \cite{ictLines} non è sempre presente la fonte da cui determinate affermazioni derivano.
Nel tracciare lo spettrogramma di questo Approfondimento saranno considerate si le problematiche specifiche incontrate nell'analisi delle varie piattaforme sia quelle generiche.
\subsection{Lo spettrogramma}
Essendo l'approfondimento incentrato sui servizi digitali,  lo spettrogramma non può che risultare particolarmente solido in corrispondenza del livello 1. In realtà sembra che l'unica preoccupazione sia proprio quella di "buttar fuori" servizi senza tenere in considerazione nessun altro livello. Di seguito le motivazioni qualitative che hanno portato alla scelta dei valori utilizzati nello spettrogramma in \cref{fig:spettro}.
\begin{description}
    \item[Livello 0] Fioco, poiché si è vista addirittura l'intromissione di un ISP che ha minato la funzionalità di un servizio.
    \item[Livello 1] Luminoso. tutto ciò che conta sembra proporre servizi.
    \item[Livello 2] Piuttosto Fioco. L'App Io è un esempio lampante di disinteresse nei confronti di questo livello.
    \item[Livello 3] Fioco. Il solo ricercare qualche informazione ufficiale richiede una non banale conoscenza, non si è guidati in alcun modo.
    \item[Livello 4] Modestamente Luminoso. I dati ci sono e spesso disponibili in formati analizzabili. Trovarli è un altro paio di maniche.
    \item[Livelli 5,6,7] Attualmente parlare di partecipazione attiva ed e-democracy equivale a parlare di fantascienza.
\end{description}
\begin{figure}[!h]
    \centering
    \caption{}
    \label{fig:spettro}
    \input{PNSD-spettro.tikz}
\end{figure}


